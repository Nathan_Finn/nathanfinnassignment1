<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    <xsl:template match="//faculty">
        <html>
            <body>
                <h2>Select and display all Heads of Faculty, School and Departments. In the output indicate what type of Head they are i.e. Faculty, School or Department.</h2>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        
                        <th>Type of Head</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                   
                    </tr>
                    
                    <xsl:for-each select="//head">
                        <tr> 
                            
                            <td><xsl:value-of select="@title" /></td>
                            <td><xsl:value-of select="firstname" /></td>
                            <td><xsl:value-of select="lastname" /></td>
                            
                        </tr>                 
                    </xsl:for-each>
                    
                    
                    
                    
                    
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>