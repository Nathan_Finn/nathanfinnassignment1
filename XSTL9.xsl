<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:template match="/">
        <html>
            <body>
                <h2>Select and present the module (details) that a particular student is taking.</h2>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        
                        <th>Student ID</th>
                        <th>First Name</th>
                        <th>Last name</th>
                        <th>Registration Status</th>
                        <th>Module Name</th>
                        <th>Module CRN</th>
                        <th>Module NFQ Level</th>
                        <th>Module Credits</th>
                        <th>Module Coordinator</th>
                        <th>Module Author</th>
                        <th>Module Lecturer Title</th>
                        <th>Module Lecturer First Name</th>
                        <th>Module Lecturer Last Name</th>
                    </tr>
                    
                    <xsl:for-each select="//module/student[@studentID='R00157992']/status/registered">
                        <tr> 
                            
                            <td><xsl:value-of select="../../@studentID" /></td>
                            <td><xsl:value-of select="../../firstname" /></td>
                            <td><xsl:value-of select="../../lastname" /></td>
                            <td><xsl:value-of select="../registered" /></td>
                            <td><xsl:value-of select="../../../@name" /></td>
                            <td><xsl:value-of select="../../../@CRN" /></td>
                            <td><xsl:value-of select="../../../@NFQ" /></td>
                            <td><xsl:value-of select="../../../@credits" /></td>
                            <td><xsl:value-of select="../../../@coordinator" /></td>
                            <td><xsl:value-of select="../../../@author" /></td>
                            <td><xsl:value-of select="../../../../module/lecturer/title" /></td>
                            <td><xsl:value-of select="../../../../module/lecturer[firstname='Donna']/firstname" /></td>
                            <td><xsl:value-of select="../../../../module/lecturer[lastname='O'&apos;Shea']/lastname" /></td>                      
                        </tr>                 
                    </xsl:for-each>   
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>