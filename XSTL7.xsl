<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:template match="/">
        <html>
            <body>
                <h2>Select and list all modules authored by “Donna O’Shea” or another lecturer of your choice.</h2>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        
                        <th>Module Name</th>
                        <th>Module Author</th>
                        
                    </tr>
                    
                    <xsl:for-each select="//programme">
                        <tr> 
                            
                            <td><xsl:value-of select="module/@name" /></td>
                            <td><xsl:value-of select="module[@author='Donna O'&apos;Shea']/@author" /></td>
                            
                            
                        </tr>                 
                    </xsl:for-each>   
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>