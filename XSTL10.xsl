<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:template match="/">
        <html>
            <body>
                <h2>Select and list the student detail for all students that are currently deferred in a particular programme</h2>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        
                        <th>Student ID</th>
                        <th>First Name</th>
                        <th>Last name</th>
                        <th>Home Address</th>
                        <th>Telephone Number</th>
                        <th>Registration Status</th>
                        <th>Programme Title</th>
                    </tr>
                    
                    <xsl:for-each select="//student/status/withdrawn">
                        <tr> 
                            
                            <td><xsl:value-of select="../../@studentID" /></td>
                            <td><xsl:value-of select="../../firstname" /></td>
                            <td><xsl:value-of select="../../lastname" /></td>
                            <td><xsl:value-of select="../../address" /></td>
                            <td><xsl:value-of select="../../telephone" /></td>
                            <td><xsl:value-of select="../withdrawn" /></td>
                            <td><xsl:value-of select="../../../../title" /></td>
                            
                            
                        </tr>                 
                    </xsl:for-each>   
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>