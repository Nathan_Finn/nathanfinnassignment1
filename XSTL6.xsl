<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:template match="/">
        <html>
            <body>
                <h2>List all modules that have a NFQ level of 9 or greater.</h2>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        
                        <th>Module Name</th>
                        <th>NFQ Level</th>
                        
                    </tr>
                    
                    <xsl:for-each select="//programme">
                        <tr> 
                            
                            <td><xsl:value-of select="module/@name" /></td>
                            <td><xsl:value-of select="module[@NFQ='Level 9 (Expert)']/@NFQ" /></td>
                            
                            
                        </tr>                 
                    </xsl:for-each>   
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>