<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:template match="/">
        <html>
            <body>
                <h2>Select and list all modules taught by the lecturer “Donna O’Shea”, or another lecturer of your choice.</h2>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        
                        <th>Module Name</th>
                        <th>Lecturer Title</th>
                        <th>Lecturer First Name</th>
                        <th>Lecturer Last Name</th>
                        
                    </tr>
                    
                    <xsl:for-each select="//programme">
                        <tr> 
                            
                            <td><xsl:value-of select="module/@name" /></td>
                            <td><xsl:value-of select="module/lecturer/title" /></td>
                            <td><xsl:value-of select="module/lecturer[firstname='Donna']/firstname" /></td>
                            <td><xsl:value-of select="module/lecturer[lastname='O'&apos;Shea']/lastname" /></td>

                            
                        </tr>                 
                    </xsl:for-each>   
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>