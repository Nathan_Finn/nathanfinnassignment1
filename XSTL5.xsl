<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:template match="/">
        <html>
            <body>
                <h2>List all registered students that are taking the module “XML in Technical Communications”, or another module of your choice.</h2>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        
                        <th>Student ID</th>
                        <th>First Name</th>
                        <th>Last name</th>
                        <th>Registration Status</th>
                        <th>Module Name</th>
                    </tr>
                    
                    <xsl:for-each select="//student/status/registered">
                        <tr> 
                            
                            <td><xsl:value-of select="../../@studentID" /></td>
                            <td><xsl:value-of select="../../firstname" /></td>
                            <td><xsl:value-of select="../../lastname" /></td>
                            <td><xsl:value-of select="../registered" /></td>
                            <td><xsl:value-of select="../../../@name" /></td>
                            
                            
                        </tr>                 
                    </xsl:for-each>   
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>