<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:template match="/">
        <html>
            <body>
                <h2>Select all programmes and their respective details that reside in the Computer Science Department.</h2>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        
                        <th>Title of Programme</th>
                        <th>Host Department</th>
                        <th>NFQ Level</th>
                        <th>Delivery Mode</th>
                        <th>Number of Semesters</th>
                        <th>Credits</th>
                        
                    </tr>
                    
                    <xsl:for-each select="//programme">
                        <tr> 
                            
                            <td><xsl:value-of select="title" /></td>
                            <td><xsl:value-of select="host" /></td>
                            <td><xsl:value-of select="NFQ" /></td>
                            <td><xsl:value-of select="delivery" /></td>
                            <td><xsl:value-of select="semesters" /></td>
                            <td><xsl:value-of select="credits" /></td>
                            
                        </tr>                 
                    </xsl:for-each>   
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>